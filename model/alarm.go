package model

import (
	"fmt"
	"github.com/google/uuid"
	"time"
)

type Alarm struct {
	VendorAssignedId string
	LocalSupplyPitId string
	AlarmId          string
	Active           bool
	Acked            bool
	DescriptionEng   string
	Type             string
	LastUpdated      string
	Time             time.Time
}

type DbAlarm struct {
	VendorAssignedId uuid.UUID  `db:"alarm.vendor_assigned_id"`
	EquipmentId      *uuid.UUID `db:"alarm.equipment_id"`
	AlarmCode        string     `db:"alarm.alarm_code"`
	Level            string     `db:"alarm.level"`
	Description      string     `db:"alarm.description"`
	Active           bool       `db:"alarm.active"`
	Acknowledged     bool       `db:"alarm.acknowledged"`
	Created          time.Time  `db:"alarm.created"`
	Received         time.Time  `db:"alarm.received"`
	Disappeared      *time.Time `db:"alarm.disappeared"`
	Updated          *time.Time `db:"alarm.updated"`
}

func NewDBAlarm(a *Alarm) *DbAlarm {
	db := &DbAlarm{
		VendorAssignedId: uuid.MustParse(a.VendorAssignedId),
		AlarmCode:        a.AlarmId,
		Level:            a.Type,
		Description:      a.DescriptionEng,
		Active:           a.Active,
		Acknowledged:     a.Acked,
		Created:          a.Time,
		Received:         a.Time,
		Updated:          nil,
		Disappeared:      nil,
	}

	if a.LocalSupplyPitId != "" && a.LocalSupplyPitId != "EMPTY" {
		id := uuid.MustParse(mapOfEquipmentId[fmt.Sprintf("%s.%s", a.VendorAssignedId, a.LocalSupplyPitId)])
		db.EquipmentId = &id
	}

	return db
}

func AlarmsContains(slice []*Alarm, val string) bool {
	for _, item := range slice {
		if val == fmt.Sprintf("%s.%s.%s", item.VendorAssignedId, item.LocalSupplyPitId, item.AlarmId) {
			return true
		}
	}
	return false
}
