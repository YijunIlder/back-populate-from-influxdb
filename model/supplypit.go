package model

const (
	dokkenVendorId        = "1c8503d4-3b21-4230-ac8f-b595cd19acd9"
	festningskaiaVendorId = "d77f124d-2bdd-4231-9370-04ced37289d4"
	nykirkekaiaVendorId   = "58a6ee75-ca28-48a4-8f7a-59439b91f160"
	skoltenVendorId       = "8a3de06b-616b-481f-84de-faa324f45594"
)

var mapOfEquipmentId = map[string]string{
	dokkenVendorId + ".1":        "d0101f9a-e902-4543-bc6e-95d683f3b84b",
	dokkenVendorId + ".2":        "7869ad02-8118-48c1-bb1d-4d29c5994641",
	dokkenVendorId + ".3":        "91268a2e-9a36-4da3-bb6b-2d28581c7623",
	dokkenVendorId + ".4":        "711fe6d4-7466-407d-b6b3-21c1043fd8c4",
	festningskaiaVendorId + ".1": "1172dcf1-756f-4b96-aae3-df6323edcea2",
	festningskaiaVendorId + ".2": "d2915d0f-c198-4c9e-890f-9cb02c046f67",
	festningskaiaVendorId + ".3": "46a30582-de8e-4692-8755-506ad877f6c1",
	nykirkekaiaVendorId + ".1":   "38a73b3d-038a-4894-a3b9-0e2f46f412b9",
	nykirkekaiaVendorId + ".2":   "ba581111-7505-4611-913d-4d507bcdffc5",
	skoltenVendorId + ".1":       "38d4b825-ce8c-4f18-aad1-7cc155bb4fe4",
	skoltenVendorId + ".2":       "f75bc3ef-b418-411e-88cf-7d59da3fa630",
	skoltenVendorId + ".3":       "294a7a35-f790-4546-941a-400c9cc5c33c",
	skoltenVendorId + ".4":       "cd5ea786-143e-4131-a084-d4c982927742",
}
