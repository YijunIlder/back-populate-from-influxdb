module gitlab.com/YijunIlder/back-populate-from-influxdb

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/influxdata/influxdb-client-go/v2 v2.5.1 // indirect
)
