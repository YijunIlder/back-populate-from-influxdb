package influx

type InfluxConfig struct {
	Url                string
	Org                string
	Bucket             string
	Token              string
	SigningCertificate string
	TimeOut            int
}
