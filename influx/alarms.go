package influx

import (
	"fmt"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"gitlab.com/YijunIlder/back-populate-from-influxdb/model"
	"strconv"
	"time"
)

func GetAlarmsFromQuery(result *api.QueryTableResult) map[string]*model.Alarm {
	amap := make(map[string]*model.Alarm)
	// Use Next() to iterate over query result lines
	total := 0
	newTotal := 0
	for result.Next() {
		total++
		v := result.Record().Values()
		rowKey := GetKeyFromRow(v)

		// If the alarm is already in the map
		if alarm, ok := amap[rowKey]; ok {
			UpdateAlarmFieldsFromRecordValue(alarm, v)
		} else {
			// New Alarm since it was not found in the map
			a := &model.Alarm{
				LocalSupplyPitId: "",
				VendorAssignedId: "",
				AlarmId:          "",
				Active:           false,
				Acked:            false,
				DescriptionEng:   "",
				Type:             "",
				LastUpdated:      "",
			}

			layout := "2006-01-02 15:04:05 +0000 UTC"
			str := fmt.Sprintf("%v", v["_time"])
			t, err := time.Parse(layout, str)

			if err != nil {
				fmt.Println(err)
			}
			a.Time = t

			if v["supply_pit_id"] != nil {
				a.LocalSupplyPitId = v["supply_pit_id"].(string)
			}

			if v["vendor_assigned_spu_id"] != nil {
				a.VendorAssignedId = v["vendor_assigned_spu_id"].(string)
			}

			if v["alarm_id"] != nil {
				a.AlarmId = v["alarm_id"].(string)
			}

			if v["\\\nsupply_pit_id"] != nil {
				a.LocalSupplyPitId = v["\\\nsupply_pit_id"].(string)
			}

			if v["\\\nvendor_assigned_spu_id"] != nil {
				a.VendorAssignedId = v["\\\nvendor_assigned_spu_id"].(string)
			}

			if v["\\\nalarm_id"] != nil {
				a.AlarmId = v["\\\nalarm_id"].(string)
			}

			UpdateAlarmFieldsFromRecordValue(a, v)
			amap[rowKey] = a
			newTotal++

		}
	}
	if result.Err() != nil {
		fmt.Printf("Query error: %s\n", result.Err().Error())
	}

	fmt.Println("total " + strconv.Itoa(total) + " alarms lines found in influx")
	fmt.Println("total " + strconv.Itoa(newTotal) + " alarms lines found in influx")
	return amap
}

// Making a key by combining alarm_id, Vendor_assigned_spu_id, supply_pit_id and time. Since each alarm should be added to influxDB with same time.
func GetKeyFromRow(row map[string]interface{}) string {
	rowKey := ""
	if row["alarm_id"] == nil {
		rowKey = fmt.Sprintf("%v%v%v%v", row["\\\nalarm_id"], row["\\\nvendor_assigned_spu_id"], row["\\\nsupply_pit_id"], row["_time"])
	} else {
		rowKey = fmt.Sprintf("%v%v%v%v", row["alarm_id"], row["vendor_assigned_spu_id"], row["supply_pit_id"], row["_time"])
	}
	return rowKey
}

// Each alarm have multiple field and value combinations, therefore multiple rows. Each Alarm should be updated for Acked, Active, Description_eng, Last_updated, Type.
func UpdateAlarmFieldsFromRecordValue(a *model.Alarm, v map[string]interface{}) {
	alarmField := v["_field"]
	alarmValue := v["_value"]
	switch alarmField {
	case "acked":
		a.Acked = alarmValue.(bool)
	case "active":
		a.Active = alarmValue.(bool)
	case "description_eng":
		if alarmValue != nil {
			a.DescriptionEng = alarmValue.(string)
		}
	case "last_updated":
		if alarmValue != nil {
			a.LastUpdated = alarmValue.(string)
		}
	case "type":
		if alarmValue != nil {
			a.Type = alarmValue.(string)
		}
	}
}
