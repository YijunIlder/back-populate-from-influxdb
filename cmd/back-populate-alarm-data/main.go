package main

import (
	"context"
	"fmt"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"gitlab.com/YijunIlder/back-populate-from-influxdb/influx"
	"gitlab.com/YijunIlder/back-populate-from-influxdb/model"
	"sort"
	"strconv"
	"time"
)

func main() {

	amap := make(map[string]*model.Alarm)
	almap := make(map[time.Time][]*model.Alarm)
	// Using backup locally, this token here is my local InfluxDB token. Please load backup in a new docker container and access locally.
	influxCfg := influx.InfluxConfig{
		Url:                "http://173.200.0.13:8086",
		Org:                "BKK",
		Bucket:             "plug",
		Token:              "IfIBQmUVsHmY2i-sZOji3IBchDw47kytaynTGY9OvR6-zzvBW8p16TD9lVxlHKnx0HCB8L-nevUyBVgArC2uMw==",
		SigningCertificate: "",
	}

	client := influxdb2.NewClient(influxCfg.Url, influxCfg.Token)
	client.Options().HTTPOptions().HTTPClient().Timeout = time.Second * 120
	defer client.Close()

	queryAPI := client.QueryAPI(influxCfg.Org)
	queryStirng := `from(bucket: "plug")
	|> range(start: 2020-01-01T00:00:00Z, stop: now())
	|> filter(fn: (r) => r["_measurement"] == "alarms")
	|> sort(columns: ["vendor_assigned_spu_id","supply_pit_id", "_time"])
	|> aggregateWindow(every: 10s, fn: last, createEmpty: false)
	|> drop(columns: ["_stop","_start", "port"])
	|> yield(name: "last")`

	// old query
	//`from(bucket: "plug")
	//|> range(start: 2020-01-01T00:00:00Z, stop: now())
	//|> filter(fn: (r) => r["_measurement"] == "alarms")`

	// Fetching raw alarm data, expect many rows/entry from InfluxDB for each alarm.
	result, err := queryAPI.Query(context.Background(), queryStirng)
	if err != nil {
		fmt.Println(err)
	}

	// Since we are reciving multiple rows for each written alarm, this function should return a map with all alarms re-created from raw data.
	amap = influx.GetAlarmsFromQuery(result)

	// Looping through alarms objects created from row data, sorted by each time to create alarmlist.
	// Each SPU sends one alarmlist.
	// SPUID-1, alarmlist: [a,b,c] at 10 am
	// SPUID-1 alarm C acked and disapeared
	// SPUID-1, alarmlist: [a,b] at 11 am
	// alarm C: started at 10 am acked at 11 am. - write to database.

	for _, alarm := range amap {
		timeKey := alarm.Time

		if _, found := almap[timeKey]; found {
			almap[timeKey] = append(almap[timeKey], alarm)
		} else {
			almap[timeKey] = []*model.Alarm{}
			almap[timeKey] = append(almap[timeKey], alarm)
		}
	}

	var listAlarm [][]*model.Alarm
	for _, alarms := range almap {
		listAlarm = append(listAlarm, alarms)
	}

	sort.Slice(listAlarm, func(i, j int) bool {
		return listAlarm[i][0].Time.Before(listAlarm[j][0].Time)
	})

	alarmKeyMap := make(map[string]*model.DbAlarm)

	previouslyAlarmList := listAlarm[0]

	for _, alarms := range listAlarm {

		currentReceivedTime := alarms[0].Time

		// deactivate alarm
		// if alarm from previously list is not present in current list is mean that it is disappeared
		for _, old := range previouslyAlarmList {
			key := fmt.Sprintf("%s.%s.%s", old.VendorAssignedId, old.LocalSupplyPitId, old.AlarmId)
			exist := model.AlarmsContains(alarms, key)
			if !exist {
				alarmKeyMap[key].Disappeared = &currentReceivedTime
			}
		}

		// add alarm to final list or update exist one
		for _, a := range alarms {
			key := fmt.Sprintf("%s.%s.%s", a.VendorAssignedId, a.LocalSupplyPitId, a.AlarmId)
			oldAlarm, ok := alarmKeyMap[key]

			if !ok {
				fmt.Println(key)
				alarmKeyMap[key] = model.NewDBAlarm(a)
				continue
			}

			oldAlarm.Active = a.Active
			oldAlarm.Acknowledged = a.Acked
			oldAlarm.Received = currentReceivedTime
		}
		previouslyAlarmList = alarms
	}

	//todo NEXT -> convert alarmKeyMap to SQL file

	fmt.Println("Map length " + strconv.Itoa(len(amap)) + " alarms from Infux")
}
